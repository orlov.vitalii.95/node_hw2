const { text } = require('express');
const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
    userId: {
        type: String,
        required: true,
        uniq: true
    },
    completed: {
        type: Boolean,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }
});

module.exports.Note = mongoose.model('Note', noteSchema);