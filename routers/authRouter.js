const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();

const { validateRegistration } = require('./middlewares/validationMiddleware');
const { login, registration } = require('../controllers/authController');

router.post('/register', validateRegistration, registration);
router.post('/login', login);

module.exports = router;