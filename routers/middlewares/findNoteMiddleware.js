const { Note } = require('../../models/noteModel');

module.exports.findNoteMiddleware = async (req, res, next) => {
    try {
        const noteId = req.params.id;
        
        if (!noteId) {
            return res.status(400).json({message: `Please, specify id for note`})
        }
        
        const note = await Note.findOne({_id: noteId, userId: req.user._id}, {__v:0})
        
        if (!note) {
            return res.status(400).json({message: `Note with id ${noteId} not found!`})
        }
        
        req.note = note;
        next();
    } catch (e) {
        res.status(500).json({message: `Internal server error`})
    }
}