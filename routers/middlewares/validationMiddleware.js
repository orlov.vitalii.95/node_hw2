const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
    const schema = Joi.object({
        username: Joi.string()
            .required(),
    
        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    });

    await schema.validateAsync(req.body);
    next();
}