const express = require('express')
const router = express.Router();
const {authMiddleware} = require('./middlewares/authMiddleware');
const {findNoteMiddleware} = require('./middlewares/findNoteMiddleware');
const {getNote, updateNote, checkNote, deleteNote, getNotes, addNote} = require('../controllers/noteController')

router.post('/', authMiddleware, addNote);
router.get('/', authMiddleware, getNotes);
router.get('/:id', authMiddleware, findNoteMiddleware, getNote);
router.put('/:id', authMiddleware, findNoteMiddleware, updateNote);
router.patch('/:id', authMiddleware, findNoteMiddleware, checkNote);
router.delete('/:id', authMiddleware, findNoteMiddleware, deleteNote);

module.exports = router;