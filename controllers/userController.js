const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt')
const { JWT_SECRET } = require('../config');
const { User } = require('../models/userModel');

module.exports.getProfileInfo = async (req, res) => {
    try {
        const userObj = await User.findOne({_id: req.user._id}).select({"password": 0, "__v": 0})
        
        res.status(200).json({user: userObj})
    } catch (e) {
        res.status(500).json({message: `Internal server error`})
    }
}

module.exports.deleteUser = async (req, res) => {
    try{        
        await User.deleteOne({_id: req.user._id})
        
        res.status(200).json({message: `Success`})
    } catch (e) {
        res.status(500).json({message: `Internal server error`})
    }
    
}

module.exports.changePassword = async (req, res) => {
    try {
        const header = req.headers['authorization'];
        const [tokenType, token] = header.split(' ');
        const {oldPassword, newPassword} = req.body;

        if (!oldPassword || !newPassword) {
            return res.status(400).json({message: `You must send old and new passwords!`})
        }
        
        
        const user = await User.findOne({_id: req.user._id});
        
        if ( !(await bcrypt.compare(oldPassword, user.password)) ) {
            return res.status(400).json({message: `Wrong password!`});
        }
        
        user.password = await bcrypt.hash(newPassword, 10);
        await user.save();
        
        res.status(200).json({message: `Success`})
    } catch (e) {
        res.status(500).json({message: `Internal server error`})
    }
}
