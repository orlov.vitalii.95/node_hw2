const { Note } = require('../models/noteModel');

module.exports.addNote = async (req, res) => {
    
    try {
        const text = req.body.text;
        
        if (!text) {
            return res.status(400).json({message: `Add some text to note!`})
        }
        
        const note = new Note({
            text,
            userId: req.user._id,
            completed: false
        })
        
        await note.save();
        
        res.status(200).json({message: 'Success'})
    } catch (e) {
        res.status(500).json({message: 'Internal server error'})
    }
}

module.exports.getNotes = async (req, res) => {
    try {
        const {offset, limit} = req.query;
        
        if (offset) {
            if (limit) {
                const notes = await Note.aggregate([
                    {$match: {userId: req.user._id}},
                    {$project: {__v: 0}},
                    {$skip: +offset},
                    {$limit: +limit}]);
                return res.status(200).json({notes : [...notes]})
            } else {
                const notes = await Note.aggregate([
                    {$match: {userId: req.user._id}},
                    {$skip: +offset}]);
                return res.status(200).json({notes : [...notes]})
            }
        }

        if (!offset && limit) {
            const notes = await Note.aggregate([
                {$match: {userId: req.user._id}},
                {$project: {__v: 0}},
                {$limit: +limit}]);
            return res.status(200).json({notes : [...notes]})
        }

        const notes = await Note.find({userId: req.user._id});
        return res.status(200).json({notes : [...notes]})
        
    } catch (e) {
        console.log(e)
        return res.status(500).json({message: `Internal server error`})
    }
}

module.exports.getNote = async (req, res) => {
    try {        
        res.status(200).json({note: req.note})
        
    } catch (e) {
        return res.status(500).json({message: `Internal server error`})
    }
}

module.exports.updateNote = async (req, res) => {
    try {      
        const text = req.body.text;
        const note = req.note;
        
        if (!text) {
            return res.status(400).json({message: `Please, specify text for note`})
        }
        
        note.text = text;
        await note.save();
        
        res.status(200).json({message: `Success`})
    } catch (e) {
        res.status(500).json({message: `Internal server error`})
    }
} 

module.exports.checkNote = async (req, res) => {
    try {
        const note = req.note;
        note.completed = !note.completed;
        await note.save();
        res.status(200).json({message: `Success`});
        
    } catch (e) {
        res.status(500).json({message: `Internal server error`})
    }
}

module.exports.deleteNote = async (req, res) => {
    try {
        const note = req.note;
        await Note.deleteOne({_id: note._id});
    res.status(200).json({message: `Success`});
    } catch (e) {
        res.status(500).json({message: `Internal server error`})
    }
}

