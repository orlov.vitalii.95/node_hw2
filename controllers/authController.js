const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { JWT_SECRET } = require('../config');
const { User } = require('../models/userModel');

module.exports.registration = async (req, res) => {
    const { username, password } = req.body;

    const isUserExists = await User.findOne({username});
    console.log(isUserExists)

    if (isUserExists) {
       return res.status(400).json({message: `User with username ${username} has been already created!`})
    }

    if (!password) {
        return res.status(400).json({message: `Password is required!`})
    }

    const user = new User({
        username,
        password: await bcrypt.hash(password, 10)
    });

    await user.save();

    res.status(200).json({message: 'Success'});
};

module.exports.login = async (req, res) => {
    const { username, password } = req.body;

    const user = await User.findOne({username});

    if (!user) {
        return res.status(400).json({message: `No user with username '${username}' found!`});
    }

    if ( !(await bcrypt.compare(password, user.password)) ) {
        return res.status(400).json({message: `Wrong password!`});
    }

    const token = jwt.sign({ username: user.username, _id: user._id }, JWT_SECRET);
    res.json({message: 'Success', jwt_token: token});
};